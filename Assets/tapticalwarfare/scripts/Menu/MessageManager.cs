﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class MessageManager : MonoBehaviour {

	public static MessageManager instance;

	public enum MessageType
	{
		NONE, LOGIN, UPDATE_SCORES, REGISTER, LOGOUT, HOST, GET_GAMES, GET_STATE, UPDATE_STATE, JOIN, END_GAME
	}

	MessageType currentRequestType = MessageType.NONE;
	RetryableWebRequest currentRequest = null;

	public bool UseVM = true;
	public int retries = 3;
	public float updateDelay = 1;
	public float retryDelay = 5;
	float delayT = 0;

	Queue<QueuedRequest> msgQueue = new Queue<QueuedRequest>();

	MenuController mCtrl;

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		OnlineContext.useVM = UseVM;
		mCtrl = MenuController.instance;
	}
	
	// Update is called once per frame
	void Update () {
		UpdateSwitch ();
	}

	void UpdateSwitch(){
		if (OnlineContext.IsConnected) {
			delayT -= Time.deltaTime;
			if (delayT <= 0) {
				delayT = updateDelay;
				UpdateMessageState ();
			}
		}
	}

	void UpdateMessageState(){
		if (currentRequestType != MessageType.NONE) {
			if (updateMessages ()) {
				
				bool success = OnlineContext.handleResponse (currentRequest);

				switch (currentRequestType) {
				case MessageType.REGISTER:
					if (success) {
						mCtrl.Notification ("New account created successfully!", mCtrl.Login);
					} else {
						mCtrl.Notification (OnlineContext.responseBody, mCtrl.SignUp);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.LOGIN:
					if (success) {
						mCtrl.LoggedIn ();
						sendMessage (MessageType.GET_GAMES);
					} else {
						mCtrl.Notification (OnlineContext.responseBody, mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.UPDATE_SCORES:
					if (!success) {
						mCtrl.Notification ("Lost connection to server.", mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.HOST:
					if (success) {
						OnlineContext.currentGame.gameId = Convert.ToInt32 (OnlineContext.responseBody);
						MenuController.startGameScene ();
					} else {
						mCtrl.Notification (OnlineContext.responseBody, mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.GET_GAMES:
					if (success) {
						if (OnlineContext.responseBody != null) {
							sendMessage (MessageManager.MessageType.UPDATE_SCORES);
							GameJson.GameList gamelist = GameJson.GameList.FromJSON (OnlineContext.responseBody);
							mCtrl.setGames (new List<GameJson> (gamelist.list));
							mCtrl.updateLoggedInViews ();
						}
					} else {
						mCtrl.Notification (OnlineContext.responseBody, mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.UPDATE_STATE:
					if (success) {
						List<GameJson> savedGames = GameJson.getLocalSaves ();
						savedGames.RemoveAt (0);
						GameJson.overwriteLocalSaves (savedGames);
						sendMessage (MessageType.GET_GAMES);
					} else {
						mCtrl.Notification ("Lost connection to server.", mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.GET_STATE:
					if (success) {
						MapState map = MapState.FromJSON (OnlineContext.responseBody);
						GameManager.mapToLoad = map;
						MenuController.startGameScene ();
					} else {
						mCtrl.Notification ("Lost connection to server.", mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.JOIN:
					if (success) {
						OnlineContext.currentGame.player2 = OnlineContext.username;
						sendMessage (MessageType.GET_STATE, OnlineContext.currentGame.gameId.ToString ());
					} else {
						mCtrl.Notification ("Lost connection to server.", mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				case MessageType.END_GAME:
					if (success) {
						sendMessage (MessageType.LOGIN);
						sendMessage (MessageType.GET_GAMES);
					} else {
						Debug.Log ("End game failed: " + OnlineContext.responseBody);
						mCtrl.Notification ("Lost connection to server.", mCtrl.Login);
						OnlineContext.IsConnected = false;
					}
					break;
				}

				if (msgQueue.Count > 0) {
					QueuedRequest qr = msgQueue.Dequeue ();
					currentRequest = qr.Request;
					currentRequestType = qr.Type;
				} else {
					currentRequest = null;
					currentRequestType = MessageType.NONE;
				}
			}
		} else {
			List<GameJson> savedGames = GameJson.getLocalSaves ();
			if (savedGames != null && savedGames.Count > 0) {
				GameJson currentGame = savedGames [0];
				sendMessage (MessageType.UPDATE_STATE, currentGame.ToJSON());
			}
		}
	}

	public void sendMessage(MessageType type){
		sendMessage (type, null);
	}

	public void getGames(){
		sendMessage (MessageType.GET_GAMES);
	}

	public void sendMessage(MessageType type, string body){
		RetryableWebRequest thisRequest = null;
		switch (type) {
		case MessageType.LOGIN:
		case MessageType.UPDATE_SCORES:
			thisRequest = OnlineContext.Login ();
			break;
		case MessageType.REGISTER:
			thisRequest = OnlineContext.SignUp ();
			break;
		case MessageType.LOGOUT:
			OnlineContext.Logout ();
			break;
		case MessageType.HOST:
			thisRequest = OnlineContext.Host (body);
			break;
		case MessageType.GET_GAMES:
			thisRequest = OnlineContext.GetGames ();
			break;
		case MessageType.UPDATE_STATE:
			thisRequest = OnlineContext.UpdateGamestate (body);
			break;
		case MessageType.GET_STATE:
			thisRequest = OnlineContext.GetGamestate (body);
			break;
		case MessageType.JOIN:
			thisRequest = OnlineContext.Join ();
			break;
		case MessageType.END_GAME:
			thisRequest = OnlineContext.EndGame ();
			break;
		}
		if (thisRequest != null) {
			if (currentRequest == null) {
				currentRequest = thisRequest;
				currentRequestType = type;
			} else {
				msgQueue.Enqueue (new QueuedRequest (type, thisRequest));
			}
		}
	}

	bool updateMessages(){
		if (!currentRequest.isDone) {
			return false;
		} else if(currentRequest.isError){
			if (currentRequest.retryCount < retries) {
				delayT = retryDelay;
				currentRequest.RetrySend ();
				return false;
			}
		}
		return true;
	}

	private class QueuedRequest {
		private MessageType type;
		private RetryableWebRequest request;

		public MessageType Type{
			get{
				return type;
			}
		}

		public RetryableWebRequest Request{
			get{
				return request;
			}
		}

		public QueuedRequest(MessageType type, RetryableWebRequest request){
			this.type = type;
			this.request = request;
		}
	}
}
