﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public static MenuController instance;

	const string GAME_PLAY = "PLAY";
	const string GAME_JOIN = "JOIN";
	const string GAME_HOST = "HOST";

	/*
	 * These represent each menu state and should never be false!
	 */
	public MenuPanel startPanel;
	public MenuPanel offlineOnlinePanel;
	public MenuPanel loginPanel;
	public MenuPanel signUpPanel;
	public MenuPanel notificationPanel;
	public MenuPanel loggedInPanel;
	public MenuPanel levelSelectPanel;
	public MenuPanel SendingRequestPanel;
	public MenuPanel GetInputPanel;

	Dictionary<string, MapState> levels = new Dictionary<string, MapState> ();
	List<GameJson> games = new List<GameJson> ();

	MessageManager msgMan = null;

	void Awake(){
		instance = this;
		UnityEngine.Object[] maps = Resources.LoadAll (MapState.MAP_DIR);
		foreach (UnityEngine.Object ta in maps) {
			if (ta is TextAsset) {
				MapState ms = MapState.FromJSON (((TextAsset)ta).text);
				levels.Add (ms.mapName, ms);
			}
		}
	}

	void Update(){
		UpdateTabInputFields ();
	}

	void UpdateTabInputFields(){
		if (Input.GetKeyDown(KeyCode.Tab)) {
			TabSelectedInputField ();
		}
	}

	public void TabSelectedInputField(){
		MenuPanel current = GetComponentInChildren<MenuPanel> ();
		InputField[] inputFields = current.GetComponentsInChildren<InputField> ();
		if (inputFields.Length > 0) {
			EventSystem system = EventSystem.current;
			for (int i = 0; i < inputFields.Length; i++) {
				if (inputFields [i].isFocused) {
					int next = i + 1;
					if (next == inputFields.Length) {
						next = 0;
					}
					system.SetSelectedGameObject (inputFields [next].gameObject);
					return;
				}
			}
			system.SetSelectedGameObject (inputFields [0].gameObject);
		}
	}

	// Use this for initialization
	public void Start () {
		msgMan = MessageManager.instance;
		purge ();
		startPanel.SetActive (true);

		int gameWinner = GameManager.gameWinner;
		if (gameWinner >= 0) {
			GameManager.gameWinner = -1;
		}
			
		if (gameWinner >= 0) {
			if (OnlineContext.IsConnected && OnlineContext.currentGame != null) {
				msgMan.sendMessage (MessageManager.MessageType.END_GAME);
				Notification ("You won!", LoggedIn);
			} else {
				Notification ("Player " + (gameWinner + 1) + " won!", Start);
			}
		} else if (OnlineContext.IsConnected && OnlineContext.currentGame != null) {
			List<GameJson> savedGames = GameJson.getLocalSaves ();
			if (savedGames == null) {
				savedGames = new List<GameJson> ();
			} 
			savedGames.Add (OnlineContext.currentGame);
			GameJson.overwriteLocalSaves (savedGames);
			OnlineContext.currentGame = null;

			Notification ("Turn Complete!", LoggedIn);
		}
	}

	public void OfflineOnline(){
		purge ();
		offlineOnlinePanel.SetActive (true);
	}

	public void Login(){
		purge ();
		clearInputFields (loginPanel);
		loginPanel.SetActive (true);
	}

	public void LoggedIn(){
		purge ();
		loggedInPanel.SetActive (true);
		Text[] texts = loggedInPanel.GetComponentsInChildren<Text> ();
		foreach (Text t in texts) { 
			if (t != null) {
				if (t.gameObject.name.Equals ("Username")) {
					t.text = OnlineContext.username + "!";
				}
				if (t.gameObject.name.Equals ("Wins")) {
					t.text = OnlineContext.wins;
				}
				if (t.gameObject.name.Equals ("Losses")) {
					t.text = OnlineContext.losses;
				}
			}
		}
		updateLoggedInViews ();
	}

	public void updateLoggedInViews(){
		DataView[] dataViews = loggedInPanel.GetComponentsInChildren<DataView> (true);
		DataView play = null;
		DataView join = null;
		DataView wait = null;
		List<string> headers = new List<string> ();
		headers.Add ("Game Id");
		headers.Add ("Player 1");
		headers.Add ("Player 2");
		foreach (DataView dv in dataViews) {
			if (dv.buttonText == "Play") {
				play = dv;
				play.ClearData ();
				play.SetHeaders (headers);
			} else if (dv.buttonText == "Join") {
				join = dv;
				join.ClearData ();
				List<string> joinHeaders = new List<string> (headers);
				joinHeaders.Add ("Password");
				join.SetHeaders (joinHeaders);
			} else if (dv.buttonText == "Wait") {
				wait = dv;
				wait.ClearData ();
				wait.SetHeaders (headers);
			} else {
				Debug.Log ("Unknown dataview: " + dv.buttonText);
			}
		}
		foreach (GameJson g in games) {
			List<string> row = new List<string> ();
			row.Add (g.gameId.ToString());
			row.Add (g.player1);
			if (g.player2 == null || g.player2.Trim ().Length == 0) {
				row.Add ("Open To Join");
			} else {
				row.Add (g.player2);
			}

			if (g.player1 == OnlineContext.username) {
				if (g.currentTurn == 0) {
					play.AddDataRow (row);
				} else {
					wait.AddDataRow (row);
				}
			} else if (g.player2 == OnlineContext.username) {
				if (g.currentTurn == 1) {
					play.AddDataRow (row);
				} else {
					wait.AddDataRow (row);
				}
			} else if (g.player2 == null || g.player2.Trim().Length == 0){
				List<string> joinRow = new List<string> (row);
				if (g.password == null || g.password.Length == 0) {
					joinRow.Add ("No");
				} else {
					joinRow.Add ("Yes");
				}
				join.AddDataRow (joinRow);
			}
		}
		join.UpdateView ();
		play.UpdateView ();
		wait.UpdateView ();
	}

	public void Logoff(){
		msgMan.sendMessage (MessageManager.MessageType.LOGOUT);
		Login ();
	}

	public void SignUp(){
		purge ();
		clearInputFields (signUpPanel);
		signUpPanel.SetActive (true);
	}

	public void Notification(string message, Action onClickAction){
		purge ();
		Button b = notificationPanel.GetComponentInChildren<Button> ();
		Text[] texts = notificationPanel.GetComponentsInChildren<Text> ();
		if (b != null) {
			b.onClick.RemoveAllListeners ();
			b.onClick.AddListener (delegate {onClickAction();});
		}
		if (texts.Length > 0) {
			foreach (Text t in texts) {
				if(t == b.GetComponentInChildren<Text>()) continue;
				t.text = message;
			}
		}
		Debug.Log (message);
		notificationPanel.SetActive (true);
	}

	public void GetInput(string message, Action onOKAction, Action onBackAction){
		purge ();
		clearInputFields (GetInputPanel);
		Button[] buttons = GetInputPanel.GetComponentsInChildren<Button> ();
		Text[] texts = GetInputPanel.GetComponentsInChildren<Text> ();
		foreach (Button b in buttons) {
			if (b != null) {
				b.onClick.RemoveAllListeners ();
				if (b.GetComponentInChildren<Text> ().text == "OK") {
					b.onClick.AddListener (delegate {
						onOKAction ();
					});
				} else if (b.GetComponentInChildren<Text> ().text == "BACK") {
					b.onClick.AddListener (delegate {
						onBackAction ();
					});
				}
			}
		}
		foreach (Text t in texts) {
			if (t.GetComponent<IsInfoText>()) {
				t.text = message;
			}
		}
		GetInputPanel.SetActive (true);
	}

	private void validateGamePasswordFromInput(){
		InputField input = GetInputPanel.GetComponentInChildren<InputField> ();
		if (input) {
			if (input.text == OnlineContext.currentGame.password) {
				JoinGame ();
			} else {
				Notification ("The password you entered is incorrect.", LoggedIn);
			}
		}
	}

	public void SendingRequest(){
		purge ();
		SendingRequestPanel.SetActive (true);
	}

	public void SubmitSignUp(){
		string username = null;
		string password = null;

		InputField[] fields = signUpPanel.GetComponentsInChildren<InputField> ();
		foreach (InputField field in fields) {
			if (field.contentType == InputField.ContentType.Standard) {
				username = field.text.Trim ();
			} else if (field.contentType == InputField.ContentType.Password) {
				password = field.text.Trim ();
			}
		}

		if (username.Length == 0) {
			Notification ("Username is missing!", SignUp);
		} else if (password.Length == 0) {
			Notification ("Password is missing!", SignUp);
		} else {
			OnlineContext.username = username;
			OnlineContext.password = password;
			msgMan.sendMessage (MessageManager.MessageType.REGISTER);
			SendingRequest ();
		}
	}

	public void SubmitLogin(){
		string username = null;
		string password = null;

		InputField[] fields = loginPanel.GetComponentsInChildren<InputField> ();
		foreach (InputField field in fields) {
			if (field.contentType == InputField.ContentType.Standard) {
				username = field.text.Trim ();
			} else if (field.contentType == InputField.ContentType.Password) {
				password = field.text.Trim ();
			}
		}

		if (username.Length == 0) {
			Notification ("Username is missing!", Login);
		} else if (password.Length == 0) {
			Notification ("Password is missing!", Login);
		} else {
			OnlineContext.username = username;
			OnlineContext.password = password;
			msgMan.sendMessage (MessageManager.MessageType.LOGIN);
			SendingRequest ();
		}
	}

	public void LevelSelect(){
		if (OnlineContext.IsConnected) {
			foreach (Transform child in levelSelectPanel.transform) {
				child.gameObject.SetActive (true);
			}
		}
		clearInputFields (levelSelectPanel);
		purge ();
		levelSelectPanel.SetActive (true);
		DataView dataView = levelSelectPanel.GetComponentInChildren<DataView> ();
		if (dataView) {
			dataView.ClearData ();

			List<String> headers = new List<String> ();
			headers.Add ("Map Name");
			dataView.SetHeaders(headers);

			foreach (MapState ms in levels.Values) {
				List<string> row = new List<string> ();
				row.Add (ms.mapName);
				dataView.AddDataRow (row);
			}

			dataView.UpdateView ();
			levelSelectPanel.SetActive (true);
		}
	}

	public void LevelSelectBack(){
		if (OnlineContext.IsConnected) {
			LoggedIn ();
		} else {
			OfflineOnline ();
		}
	}

	private void clearInputFields(MenuPanel panel){
		InputField[] fields = panel.GetComponentsInChildren<InputField> ();
		foreach (InputField field in fields) {
			field.text = "";
		}
	}

	private void purge(){
		startPanel.SetActive (false);
		offlineOnlinePanel.SetActive (false);
		loginPanel.SetActive (false);
		signUpPanel.SetActive (false);
		notificationPanel.SetActive(false);
		loggedInPanel.SetActive (false);
		levelSelectPanel.SetActive (false);
		SendingRequestPanel.SetActive (false);
		GetInputPanel.SetActive (false);
	}

	public static void JoinGame(){
		if (OnlineContext.currentGame != null) {
			instance.msgMan.sendMessage (MessageManager.MessageType.JOIN, null);
			instance.SendingRequest ();
		}
	}

	public static void ProcessGameAction (string action, string gameId){
		switch (action.ToUpper()) {
		case GAME_JOIN:
			foreach (GameJson game in instance.games) {
				if (Convert.ToInt32 (gameId) == game.gameId) {
					OnlineContext.currentGame = game;
					break;
				}
			}
			if (OnlineContext.currentGame.password == null || OnlineContext.currentGame.password.Length == 0) {
				JoinGame ();
			} else {
				instance.GetInput ("The game you have selected requires a password. Please enter the password below:", instance.validateGamePasswordFromInput, instance.LoggedIn);
			}
			break;
		case GAME_PLAY:
			if (OnlineContext.IsConnected) {
				int n;
				if (!int.TryParse(gameId, out n)) {
					GameJson gameData = new GameJson(0, OnlineContext.username, null, 0); //new game
					InputField[] textFields = instance.levelSelectPanel.GetComponentsInChildren<InputField>();
					foreach (InputField tf in textFields) {
						if (tf.contentType == InputField.ContentType.Password) {
							if (tf.text != null && tf.text.Trim().Length > 0) {
								gameData.password = tf.text.Trim();
							}
						}
					}
					
					MapState map = instance.getMapByName(gameId);
					GameManager.mapToLoad = map;
					gameData.gameState = map.ToJSON();
					OnlineContext.currentGame = gameData;
					instance.msgMan.sendMessage (MessageManager.MessageType.HOST, gameData.ToJSON());
					instance.SendingRequest ();
				} else {
					foreach (GameJson game in instance.games) {
						if (Convert.ToInt32 (gameId) == game.gameId) {
							OnlineContext.currentGame = game;
							break;
						}
					}
					instance.msgMan.sendMessage (MessageManager.MessageType.GET_STATE, gameId.ToString ());
					instance.SendingRequest ();
				}
			} else {
				GameManager.mapToLoad = instance.getMapByName (gameId);
				startGameScene ();
			}
			break;
		default:
			Debug.Log ("Unrecognized Game Action: " + action + ".");
			break;
		}
		Debug.Log (action.ToString() + " " + gameId);
	}

	public static void startGameScene(){
		SceneManager.LoadScene ("gameScene");
	}

	public MapState getMapByName(string mapName){
		if (instance.levels.ContainsKey (mapName)) {
			return levels[mapName];
		}
		return null;
	}

	public void setGames(List<GameJson> games){
		this.games = games;
	}
}
