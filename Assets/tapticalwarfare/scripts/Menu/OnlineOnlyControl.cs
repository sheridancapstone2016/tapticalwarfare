﻿using UnityEngine;
using System.Collections;

public class OnlineOnlyControl : MonoBehaviour {

	// Use this for initialization
	void Update () {
		UpdateVisible ();
	}

	void UpdateVisible(){
		if (!OnlineContext.IsConnected){
			gameObject.SetActive (false);
		}
	}
}
