﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Text;
using System.Collections.Generic;

public class OnlineContext
{

	public static bool useVM = true;

	//HTTP Methods
	private const string POST = "POST";
	private const string GET = "GET";
	private const string DELETE = "DELETE";

	private class Headers
	{
		//Headers
		public const string USERNAME = "username";
		public const string PASSWORD = "password";
		public const string GAME = "game";
		public const string JOIN = "join";
		public const string HOST = "host";
		public const string UPDATE = "update";
		public const string GAME_PASSWORD = "game_password";
		public const string GAME_ID = "game_id";
		public const string GAME_WIN = "game_win";
		public const string GAME_LOSE = "game_lose";
	}

	//URIs
	private const string LOCAL = "http://localhost:8080/TapticalWarfareServer/";
	private const string VM = "http://142.55.32.86:50101/TapticalWarfareServer/";
	private const string URI_LOGIN = "Login";
	private const string URI_REGISTER = "Register";
	private const string URI_GAME = "Game";

	public static string username = null;
	public static string password = null;
	public static string wins = "0";
	public static string losses = "0";
	public static string responseBody = null;
	public static GameJson currentGame = null;

	public static bool handleResponse (RetryableWebRequest r)
	{
		if (r.responseCode == 204) {
			Dictionary<string, string> headers = r.GetResponseHeaders ();
			if(headers.ContainsKey(Headers.GAME_WIN)){
				wins = headers [Headers.GAME_WIN];
			}
			if(headers.ContainsKey(Headers.GAME_LOSE)){
				losses = headers [Headers.GAME_LOSE];
			}
			responseBody = null;
			return true;
		}
		if (r.responseCode == 200) {
			responseBody = System.Text.Encoding.UTF8.GetString (r.downloadHandler.data, 0, r.downloadHandler.data.Length);
			return true;
		}
		if (r.isError) {
			responseBody = "Unable to contact server!\nPlease try again later.";
			IsConnected = false;
		} else if (r.responseCode >= 400) {
			responseBody = System.Text.Encoding.UTF8.GetString (r.downloadHandler.data, 0, r.downloadHandler.data.Length);
		} else {
			responseBody = "Unkown Error!";
			Debug.Log ("***ERROR*** UNHANDLED RESPONSE CODE: " + r.responseCode);
		}
		return false;
	}

	public static RetryableWebRequest Login ()
	{
		RetryableWebRequest r = getRequest (URI_LOGIN, GET);
		r.Send ();
		return r;
	}

	public static RetryableWebRequest SignUp ()
	{
		RetryableWebRequest r = getRequest (URI_REGISTER, POST);
		r.Send ();
		return r;
	}

	public static void Logout ()
	{
		RetryableWebRequest r = getRequest (URI_LOGIN, DELETE);
		r.Send ();
		IsConnected = false;
	}

	public static RetryableWebRequest Host (string body)
	{
		RetryableWebRequest r = getRequest (URI_GAME, POST);
		r.SetRequestHeader (Headers.GAME, Headers.HOST);
		r.body = body;
		r.Send ();
		return r;
	}

	public static RetryableWebRequest Join(){
		RetryableWebRequest r = getRequest (URI_GAME, POST);
		r.SetRequestHeader (Headers.GAME, Headers.JOIN);
		r.SetRequestHeader (Headers.GAME_ID, currentGame.gameId.ToString());
		if (currentGame.password != null && currentGame.password.Length > 0) {
			r.SetRequestHeader (Headers.GAME_PASSWORD, currentGame.password);
		}
		r.Send ();
		return r;
	}

	public static RetryableWebRequest GetGames(){
		RetryableWebRequest r = getRequest (URI_GAME, GET);
		r.Send ();
		return r;
	}

	public static RetryableWebRequest GetGamestate(string gameId){
		RetryableWebRequest r = getRequest (URI_GAME, GET);
		r.SetRequestHeader (Headers.GAME_ID, gameId);
		r.Send ();
		return r;
	}

	public static RetryableWebRequest UpdateGamestate(string body){
		RetryableWebRequest r = getRequest (URI_GAME, POST);
		r.SetRequestHeader (Headers.GAME, Headers.UPDATE);
		r.body = body;
		r.Send ();
		return r;
	}

	public static RetryableWebRequest EndGame(){
		RetryableWebRequest r = getRequest (URI_GAME, POST);
		r.SetRequestHeader (Headers.GAME_ID, currentGame.gameId.ToString());
		r.SetRequestHeader (Headers.GAME, Headers.GAME_WIN);
		if (currentGame.player1 == username) {
			r.SetRequestHeader (Headers.GAME_LOSE, currentGame.player2);
		} else if (currentGame.player2 == username) {
			r.SetRequestHeader (Headers.GAME_LOSE, currentGame.player1);
		}
		r.Send ();
		currentGame = null;
		return r;
	}

	public static bool IsConnected {
		get {
			if (username != null) {
				return true;
			}
			return false;
		}
		set {
			if (!value) {
				username = null;
				password = null;
				responseBody = null;
				currentGame = null;
			}
		}
	}

	private static RetryableWebRequest getRequest (string uri, string method)
	{
		RetryableWebRequest r = new RetryableWebRequest ();
		if (useVM) {
			r.url = VM + uri;
		} else {
			r.url = LOCAL + uri;
		}
		r.method = method;
		r.SetRequestHeader (Headers.USERNAME, username);
		r.SetRequestHeader (Headers.PASSWORD, password);
		return r;
	}
}
