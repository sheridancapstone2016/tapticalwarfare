﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class DataView : MonoBehaviour {

	//prefabs
	public GameObject dataTable;
	public GameObject dataCell;
	public GameObject headerCell;
	public GameObject button;

	public string emptyText = "The table is currently empty";
	public string buttonText = "ButtonText";

	private List<string> mHeaders = new List<string>();
	private List<List<string>> mData = new List<List<string>>();
	private GridLayoutGroup glayout;
	private RectTransform rt;

	// Use this for initialization
	void Awake () {
		
		glayout = dataTable.GetComponent<GridLayoutGroup> ();
		rt = dataTable.transform.parent.GetComponent<RectTransform> ();
		UpdateView ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void SetHeaders(List<string> headers){
		mHeaders = headers;
	}

	public void SetData(List<List<string>> data){
		mData = data;
	}
		
	public void AddDataRow(List<string> row){
		mData.Add (row);
	}

	public void UpdateView(){
		ClearViewData ();

		if (mData != null && mData.Count > 0) {
			int colCount = button != null ? mHeaders.Count + 1 : mHeaders.Count;
			if (mHeaders != null && mHeaders.Count > 0) {
				//GameObject hr = Instantiate (headerRow, dataTable);
				if (glayout != null) {
					glayout.constraintCount = colCount;
					if (rt != null) {
						glayout.cellSize = new Vector2(rt.rect.width / colCount, glayout.cellSize.y);
					}
				} 
				for (int i = 0; i < colCount; i++) {
					GameObject hd = Instantiate (headerCell, dataTable);
					Text t = hd.GetComponentInChildren<Text> ();
					if (i < mHeaders.Count) {
						//create empty header for button column
						t.text = mHeaders [i];
					} else {
						t.text = "";
					}
				}
			}

			foreach (List<string> row in mData) {
				//GameObject tr = Instantiate (dataRow, dataTable);
				for(int i = 0; i < colCount; i++) {
					if (i < row.Count) {
						GameObject td = Instantiate (dataCell, dataTable);
						Text t = td.GetComponent<Text> ();
						t.text = row [i];
					} else {
						GameObject btnObj = Instantiate (button, dataTable);
						Text t = btnObj.GetComponentInChildren<Text> ();
						t.text = buttonText;
						Button btn = btnObj.GetComponent<Button> ();
						string index = row [0];
						btn.onClick.AddListener (delegate {MenuController.ProcessGameAction(buttonText, index);});
					}
				}
			}
		} else {
			if (glayout != null) {
				glayout.constraintCount = 1;
				if (rt != null) {
					glayout.cellSize = new Vector2(rt.rect.width, glayout.cellSize.y);
				}
			} 
			GameObject td = Instantiate (dataCell, dataTable);
			Text t = td.GetComponent<Text> ();
			t.text = emptyText;
		}
	}

	GameObject Instantiate (GameObject type, GameObject parent){
		return (GameObject)Instantiate (type, parent.transform, false);
	}

	public void ClearData(){
		mHeaders = new List<String> ();
		mData = new List<List<String>>();
	}

	void ClearViewData(){
		foreach (Transform child in dataTable.transform) {
			GameObject.Destroy (child.gameObject);
		}
	}
}
