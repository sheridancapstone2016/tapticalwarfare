﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/*
 * Wrapper for UnityWebRequest which will be able to be retried after a send attempt has failed
 */
using System.Collections.Generic;
using System.Text;


public class RetryableWebRequest {

	UnityWebRequest request;
	Dictionary<string, string> headers;
	string requestBody = null;
	int retries = 0;

	public RetryableWebRequest(){
		request = new UnityWebRequest ();
		headers = new Dictionary<string, string> ();
		request.downloadHandler = new DownloadHandlerBuffer ();
	}

	public int retryCount{
		get{
			return retries;
		}
	}

	public string url{
		get{
			return request.url;
		}
		set{
			request.url = value;
		}
	}

	public string method{
		get{
			return request.method;
		}
		set{
			request.method = value;
		}
	}

	public string body{
		get{
			return requestBody;
		}
		set{
			requestBody = value;
		}
	}

	public bool isDone{
		get{
			return request.isDone;
		}
	}

	public bool isError{
		get{
			return request.isError;
		}
	}

	public long responseCode{
		get{
			return request.responseCode;
		}
	}

	public DownloadHandler downloadHandler{
		get{
			return request.downloadHandler;
		}
	}

	public Dictionary<string, string> GetResponseHeaders(){
		return request.GetResponseHeaders();
	}

	public void SetRequestHeader (string name, string value){
		headers [name] = value;
		request.SetRequestHeader (name, value);
	}

	public void Send(){
		if (body != null) {
			request.uploadHandler = new UploadHandlerRaw (Encoding.ASCII.GetBytes (body));
			request.uploadHandler.contentType = "application/json";
		}
		Debug.Log ("first send");
		request.Send ();
	}

	public void RetrySend(){
		UnityWebRequest retry = new UnityWebRequest ();
		retry.url = request.url;
		retry.method = request.method;
		retry.downloadHandler = new DownloadHandlerBuffer ();

		if (body != null) {
			retry.uploadHandler = new UploadHandlerRaw (Encoding.ASCII.GetBytes (body));
			retry.uploadHandler.contentType = "application/json";
		}

		foreach (string key in headers.Keys) {
			retry.SetRequestHeader (key, headers [key]);
		}

		retries++;
		Debug.Log ("Retry: " + retries);

		this.request = retry;
		request.Send ();
	}
}
