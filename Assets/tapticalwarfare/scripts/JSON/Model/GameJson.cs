﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

[Serializable]
public class GameJson : JsonObject {

	[Serializable]
	public class GameList : JsonObject {
		public GameJson[] list;

		public GameList(){

		}

		public GameList(GameJson[] list){
			this.list = list;
		}

		public static GameList FromJSON(string json){
			return JsonUtility.FromJson<GameList>(json);
		}
	}

	public int gameId;
	public String player1;
	public String player2;
	public int currentTurn;
	public string gameState;
	public string password;

	public const string saveFile = "game.dat";

	public GameJson(){

	}

	public GameJson(int gameId, String player1, String player2, int currentTurn){
		this.gameId = gameId;
		this.player1 = player1;
		this.player2 = player2;
		this.currentTurn = currentTurn;
	}

	public static GameJson FromJSON(string json){
		return JsonUtility.FromJson<GameJson>(json);
	}

	public static List<GameJson> getLocalSaves(){
		if (File.Exists (saveFile)) {
			StreamReader reader = new StreamReader (saveFile);
			string fileContent = reader.ReadToEnd ();
			reader.Close ();
			if (fileContent != null && fileContent.Length > 0) {
				return new List<GameJson>(GameList.FromJSON (fileContent).list);
			}
		}
		return null;
	}

	public static void overwriteLocalSaves(List<GameJson> games){
		if (games != null) {
			StreamWriter writer = new StreamWriter (saveFile, false);
			GameList gl = new GameList ();
			gl.list = games.ToArray ();
			writer.WriteLine (gl.ToJSON ());
			writer.Close ();
		} else {
			StreamWriter writer = new StreamWriter (saveFile, false);
			writer.WriteLine ("");
			writer.Close ();
		}
	}
}
