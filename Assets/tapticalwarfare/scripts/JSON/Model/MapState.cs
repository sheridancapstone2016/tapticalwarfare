﻿using System;
using UnityEngine;

[Serializable]
public class MapState : JsonObject{

	/*
	 * JSON object structure definition:
	 */
	[Serializable]
	public class MapPlayer {

		[Serializable]
		public class MapPlayerProperty {
			public int id;
			public string type;
			public int x;
			public int y;
			public int hp;
		}
		public MapPlayerProperty[] properties;
	}

	[Serializable]
	public class MapTerrain {

		public string type;
		public int x;
		public int y;
	}

	/*
	 * Beginning of Map class definition:
	 */

	public const string MAP_DIR = "maps/";

	public string mapName;
	public int height;
	public int width;
	public MapPlayer player1;
	public MapPlayer player2;
	public int player1Resources;
	public int player2Resources;
	public MapTerrain[] terrain;

	public MapState(string mapName, int height, int width){
		this.mapName = mapName;
		this.height = height;
		this.width = width;
		this.player1 = new MapPlayer ();
		this.player2 = new MapPlayer ();
		this.player1Resources = -1;
		this.player2Resources = -1;
	}

	public static MapState FromJSON(string json){
		return JsonUtility.FromJson<MapState>(json);
	}

	public static MapState FromFile(string path){
		string filePath = path.Replace(".json", "");

		TextAsset targetFile = Resources.Load<TextAsset>(MAP_DIR + filePath);

		if (targetFile != null) {
			return MapState.FromJSON (targetFile.text);
		} else {
			Debug.Log (filePath + " does not exist");
		}
		return null;
	}


}
