﻿using UnityEngine;
using System;

[Serializable]
public abstract class JsonObject {

	public static string ToJSON(JsonObject job){
		return ToJSON (job, false);
	}

	public static string ToJSON(JsonObject job, bool pretty){
		return JsonUtility.ToJson(job, pretty);
	}

	public string ToJSON(){
		return JsonObject.ToJSON (this);
	}
}
