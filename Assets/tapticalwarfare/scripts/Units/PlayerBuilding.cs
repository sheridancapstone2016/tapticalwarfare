﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerBuilding : Building
{
	
    public bool impassible = false;
    private GameObject mM;
	SpriteRenderer rend;

    // Use this for initialization
    void Start()
    {
		GameManager gm = GameManager.instance;
		Tile t = gm.map.getTileByPos (this.gridCoordinates);

		if (t) {
			t.ShallNotPass = true;
		}

		rend = GetComponentInChildren<SpriteRenderer> ();
    }

    // Update is called once per frame
    public override void Update()
    {
		if (rend) {
			if (completeTurn) {
				rend.material.color = Color.grey;
			} else {
				rend.material.color = Color.white;
			}
		}
		base.Update ();
    }

    override public void TurnOnGUI()
    {
        base.TurnOnGUI();
    }


    void OnMouseDown()
    {
		GameManager gm = GameManager.instance;
		if (gm.currentPlayerIndex >= 0 && gm.currentPlayerIndex < gm.map.units.Count)
        {
			if (gm.map.units[gm.currentPlayerIndex].attacking)
            {
                gm.AttackBuildingWithCurrentPlayer(this);
            }
        }
        if (!completeTurn)
        {
            if (team == gm.currentPlayerTurn)
            {
                gm.clearSelection();
                gm.removeTileHighlights();
                selected = true;
            }
        }
    }
}
