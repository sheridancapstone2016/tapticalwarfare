using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour {

	public UnitType unitType;
	public int id;

	public Vector2 gridCoordinates = Vector2.zero;
	
	public Vector3 moveDestination;
	public float moveSpeed = 10.0f;

	public bool moving = false;
	public bool attacking = false;
    public bool building = false;
    public bool back = false;
    public bool back2 = false;
    public bool attackAnim = false;

    public int team = 0;

	public int HP = 25;

	public int movementRange = 5;
	public int attackRange = 1;
	public int attackDamage = 1;

    public bool dead = false;

	public UnitType construct;
    public bool groundOrAir;

    GameObject GM;
    SoundManager SM;

    /*
    Unit Types:
    0 - Builder
    1 - Anti-Air
    2 - Mech
    3 - Tank
    4 - Bomber Aircraft
    5 - Helicopter
    6 - Jet
    */

	public int cost = 500;

	bool doneMove;
	public bool done;
    public bool completeTurn;
    public bool hasMoved;

    public bool selected = false;


	//movement animation
	public List<Vector3> positionQueue = new List<Vector3>();	
	//

    void Start()
    {
        GM = GameObject.Find("GameManager");
        SM = this.GetComponent<SoundManager>();
    }

	void Awake () {
		moveDestination = transform.position;
	}
	
	// Update is called once per frame
	public virtual void Update () {		
		if (completeTurn) {
			selected = false;
		}
		if (HP <= 0) {
            dead = true;
            this.gameObject.transform.position = new Vector3(-20, 20, -20);
		}
	}

	public virtual void TurnUpdate () {
		if (done) {
			done = false;
			doneMove = false;
			moving = false;
			attacking = false;
            back = false;
            back2 = false;
			GameManager.instance.nextTurn();
		}
	}

	public void finishTurn(){
		done = true;
	}

	public bool DoneMove{
		get{
			return doneMove;
		}
		set{
			doneMove = value;
		}
	}

	public virtual void TurnOnGUI () {
		
	}

	public void OnGUI() {
		//display HP
		Vector3 location = Camera.main.WorldToScreenPoint(transform.position) + Vector3.up * 35;
		GUIStyle guiStyle = new GUIStyle("button");
		guiStyle.fontSize = (int)(12 * (Screen.height / 700f));

		if (!selected) {
			Rect hpRect = new Rect();
			hpRect.width = (int)(30f * (Screen.width / 1200f));
			hpRect.height = (int)(20f * (Screen.height / 700f));
			hpRect.x = location.x;
			hpRect.y = Screen.height - location.y;

			GUI.Label (hpRect, HP.ToString (), guiStyle);
		}
        //Display action buttons
		int buttonCount = 0;
		Rect buttonRect = new Rect ();
        if (selected)
        {
            GameManager.instance.unitOrBuilding = 0;
			buttonRect.height = (int)(20f * (Screen.height / 700f));
			buttonRect.width = (int)(80f * (Screen.width / 1200f));
			buttonRect.x = location.x;

            if (back == true || back2 == true)
            {
				//Select which building to construct
                if (back2 == true)
                {
                    buttonRect.width = (int)(80f * (Screen.width / 1000f));
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button(buttonRect, "Resource - 250", guiStyle))
                    {
						construct = UnitType.RE;
                        back2 = false;
                        GameManager.instance.highlightTilesAt(gridCoordinates, Color.green, 1, false);
                    }
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button(buttonRect, "Factory - 500", guiStyle))
                    {
						construct = UnitType.FA;
                        back2 = false;
                        GameManager.instance.highlightTilesAt(gridCoordinates, Color.green, 1, false);
                    }
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button(buttonRect, "Airport - 750", guiStyle))
                    {
						construct = UnitType.AI;
                        back2 = false;
                        GameManager.instance.highlightTilesAt(gridCoordinates, Color.green, 1, false);
                    }
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button(buttonRect, "Wall - 100", guiStyle))
                    {
						construct = UnitType.WA;
                        back2 = false;
                        GameManager.instance.highlightTilesAt(gridCoordinates, Color.green, 1, false);
                    }
                }
				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (GUI.Button(buttonRect, "Back", guiStyle) || Input.GetKeyDown(KeyCode.Escape))
				{
					moving = false;
					attacking = false;
					building = false;
					back = false;
					back2 = false;
					GameManager.instance.removeTileHighlights();
				}
            }
            else
            {
                //Move button
                if (!hasMoved)
                {
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button(buttonRect, "Move", guiStyle))
                    {
                        if (selected)
                        {
                            GameManager.instance.removeTileHighlights();
                            moving = true;
                            attacking = false;
                            building = false;
                            back = true;
                            back2 = false;
                            Color myColor = new Color();
                            ColorUtility.TryParseHtmlString("#0033ff", out myColor);
                            GameManager.instance.highlightTilesAt(gridCoordinates, myColor, movementRange, false);
                        }
                        else
                        {
                            moving = false;
                            attacking = false;
                            building = false;
                            back = false;
                            back2 = false;
                            GameManager.instance.removeTileHighlights();
                        }
                    }
                }

                //Attack button
				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (GUI.Button(buttonRect, "Attack", guiStyle))
                {
                    if (!attacking)
                    {
                        GameManager.instance.removeTileHighlights();
                        moving = false;
                        attacking = true;
                        building = false;
                        back = true;
                        back2 = false;
                        GameManager.instance.highlightTilesAt(gridCoordinates, Color.red, attackRange);
                    }
                    else
                    {
                        moving = false;
                        attacking = false;
                        building = false;
                        back = false;
                        back2 = false;
                        GameManager.instance.removeTileHighlights();
                    }
                }

				if (unitType == UnitType.BU)
                {
                    //Build button - This will only apply to builders
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button(buttonRect, "Build", guiStyle))
                    {
                        if (!building)
                        {
                            GameManager.instance.removeTileHighlights();
                            moving = false;
                            attacking = false;
                            building = true;
                            back = true;
                            back2 = true;
                        }
                        else
                        {
                            moving = false;
                            attacking = false;
                            building = false;
                            back = false;
                            back2 = false;
                            GameManager.instance.removeTileHighlights();
                        }
                    }
                }
                //End unit turn
				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (GUI.Button(buttonRect, "Wait", guiStyle))
                {
                    GameManager.instance.removeTileHighlights();
                    DoneMove = false;
                    moving = false;
                    attacking = false;
                    back = false;
                    back2 = false;
                    completeTurn = true;
                    GameManager.instance.nextTurn();
                }

				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (GUI.Button(buttonRect, "Back", guiStyle) || Input.GetKeyDown(KeyCode.Escape))
				{
					moving = false;
					attacking = false;
					building = false;
					back = false;
					back2 = false;
					selected = false;
					GameManager.instance.removeTileHighlights();
				}
            }
        }
	}
}
