﻿using UnityEngine;
using System.Collections;

public enum UnitType
{
	BU,
	//builder
	AA,
	//anti air
	ME,
	//mech
	TA,
	//tank
	BO,
	//bomber
	HE,
	//helicopter
	JE,
	//jet
	HQ,
	//head quarters
	RE,
	//resource
	FA,
	//factory
	AI,
	//airport
	WA
	//wall
}
