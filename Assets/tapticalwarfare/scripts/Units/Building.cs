﻿using UnityEngine;
using System.Collections;

public class Building : MonoBehaviour
{
	public UnitType unitType;

	public int id;

	GameManager gm = GameManager.instance;

    public Vector2 gridCoordinates = Vector2.zero;

    public bool attacking = false;
    public bool shopping = false;
    public bool building = false;
    public bool back = false;
    public bool back2 = false; //Building
    public bool back3 = false; //Constructing

    public bool upgradeHealth = false;
    public bool upgradeAttack = false;
    public bool upgradeMove = false;

    public int team = 0;

    public int HP = 100;

    public int attackRange = 1;
    public int attackDamage = 1;
    public int buildingRange = 1;

    public bool dead = false;

	public UnitType construct;
    public bool hqDestroyed = false;

    bool doneMove;
    public bool done;
    public bool completeTurn;

    public bool selected = false;

    GameObject GM;
    ResourceManager RM;
    SoundManager SM;// = GameObject.Find("GameManager").GetComponent<SoundManager>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
	public virtual void  Update()
    {
		if (completeTurn) {
			selected = false;
		}
		if (HP <= 0) {
			dead = true;
			Tile t = gm.map.getTileByPos(this.gridCoordinates);
			t.ShallNotPass = false;
			this.gameObject.transform.position = new Vector3(-20, 20, -20);
		}
    }

    public virtual void TurnUpdate()
    {
        if (done)
        {
            done = false;
            doneMove = false;
            attacking = false;
            GameManager.instance.nextTurn();
        }
    }

    public void finishTurn()
    {
        done = true;
    }

    public bool DoneMove
    {
        get
        {
            return doneMove;
        }
        set
        {
            doneMove = value;
        }
    }

    public virtual void TurnOnGUI()
    {

    }

    public void OnGUI()
    {
		Vector3 location = Camera.main.WorldToScreenPoint(transform.position) + Vector3.up * 35;
		GUIStyle guiStyle = new GUIStyle("button");
		guiStyle.fontSize = (int)(12 * (Screen.height / 700f));

		//Only print heath when not selected
		if (!selected) {
			Rect hpRect = new Rect();
			hpRect.width = (int)(30f * (Screen.width / 1200f));
			hpRect.height = (int)(20f * (Screen.height / 700f));
			hpRect.x = location.x;
			hpRect.y = Screen.height - location.y;

			GUI.Label (hpRect, HP.ToString (), guiStyle);
		}
		Rect buttonRect = new Rect ();
		int buttonCount = 0;
		if (selected) {
			GameManager.instance.unitOrBuilding = 1;
			buttonRect.height = (int)(20f * (Screen.height / 700f));
			buttonRect.width = (int)(80f * (Screen.width / 1200f));
			buttonRect.x = location.x;
			if (back == true || back2 == true) {
				if (back2 == true) {
                    buttonRect.width = (int)(80f * (Screen.width / 900f));
					if (unitType == UnitType.HQ) {
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Builder - 500", guiStyle)) {
							construct = UnitType.BU;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
					} else if (unitType == UnitType.FA) {
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Anti-Air - 1000", guiStyle)) {
							construct = UnitType.AA;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Mech - 1000", guiStyle)) {
							construct = UnitType.ME;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Tank - 1500", guiStyle)) {
							construct = UnitType.TA;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
					} else if (unitType == UnitType.AI) {
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Bomber - 2000", guiStyle)) {
							construct = UnitType.BO;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Helicopter - 1500", guiStyle)) {
							construct = UnitType.HE;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
						buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
						if (GUI.Button (buttonRect, "Jet - 1750", guiStyle)) {
							construct = UnitType.JE;
							back2 = false;
							GameManager.instance.highlightTilesAt (gridCoordinates, Color.green, 1, false);
						}
					}
				}
				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (GUI.Button (buttonRect, "Back", guiStyle) || Input.GetKeyDown (KeyCode.Escape)) {
					attacking = false;
					building = false;
					back = false;
					back2 = false;
                    back3 = false;
					GameManager.instance.removeTileHighlights ();
				}
            } else if (back3 == true) {
                //Upgrade functionality
                //The button will not appear if you have the upgrade already
                buttonRect.width = (int)(80f * (Screen.width / 900f));
                if (!upgradeHealth)
                {
                    buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
                    if (GUI.Button(buttonRect, "Health + 5 - 3000", guiStyle))
                    {
                        RM = GameObject.Find("GameManager").GetComponent<ResourceManager>();
                        if (RM.purchasable(3000, team))
                        {
                            RM.spend(3000, team);
                            upgradeHealth = true;
                        }
                        back3 = false;
                    }
                }
                if (!upgradeAttack)
                {
                    RM = GameObject.Find("GameManager").GetComponent<ResourceManager>();
                    buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
                    if (GUI.Button(buttonRect, "Attack + 5 - 3000", guiStyle))
                    {
                        if (RM.purchasable(3000, team))
                        {
                            RM.spend(3000, team);
                            upgradeAttack = true;
                        }
                        back3 = false;
                    }
                }
                if (!upgradeMove)
                {
                    RM = GameObject.Find("GameManager").GetComponent<ResourceManager>();
                    buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
                    if (GUI.Button(buttonRect, "Move + 1 - 6000", guiStyle))
                    {
                        if (RM.purchasable(6000, team))
                        {
                            RM.spend(6000, team);
                            upgradeMove = true;
                        }
                        back3 = false;
                    }
                }

                buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
                if (GUI.Button(buttonRect, "Back", guiStyle) || Input.GetKeyDown(KeyCode.Escape))
                {
                    attacking = false;
                    building = false;
                    back = false;
                    back2 = false;
                    back3 = false;
                    GameManager.instance.removeTileHighlights();
                }
			} else {

				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (unitType == UnitType.HQ || unitType == UnitType.FA || unitType == UnitType.AI) {
                    //Upgrade unit feature for Factories and Airports
                    if (unitType == UnitType.FA || unitType == UnitType.AI)
                    {
                        if (GUI.Button(buttonRect, "Upgrade", guiStyle))
                        {
                            attacking = false;
                            building = false;
                            back = false;
                            back2 = false;
                            back3 = true;
                        }
                        buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
                    }

					//Build button - This will only apply to buildings that can construct units
					if (GUI.Button (buttonRect, "Build", guiStyle)) {
						if (!building) {
							GameManager.instance.removeTileHighlights ();
							attacking = false;
							building = true;
							back = true;
							back2 = true;
                            back3 = false;
						} else {
							attacking = false;
							building = false;
							back = false;
							back2 = false;
                            back3 = false;
							GameManager.instance.removeTileHighlights ();
						}
					}
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);

					//End unit turn
					if (GUI.Button (buttonRect, "Wait", guiStyle)) {
						GameManager.instance.removeTileHighlights ();
						DoneMove = false;
						attacking = false;
						back = false;
						back2 = false;
                        back3 = false;
						completeTurn = true;
						GameManager.instance.nextTurn ();
					}
				} else { //lets allow Resources and Walls to be destroyed
					buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
					if (GUI.Button (buttonRect, "Destroy", guiStyle)) {
						GameManager.instance.removeTileHighlights ();
						DoneMove = false;
						attacking = false;
						back = false;
						back2 = false;
                        back3 = false;
						completeTurn = true;
						//gridCoordinates = new Vector2 (-1, -1);
						HP = 0;
						GameManager.instance.nextTurn ();
					}
				}
				buttonRect.y = Screen.height - location.y + (buttonRect.height * buttonCount++);
				if (GUI.Button (buttonRect, "Back", guiStyle) || Input.GetKeyDown (KeyCode.Escape)) {
					attacking = false;
					building = false;
					back = false;
					back2 = false;
                    back3 = false;
					selected = false;
					GameManager.instance.removeTileHighlights ();
				}
			}
		} 
    }
}
