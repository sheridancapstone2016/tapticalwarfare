using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

public class PlayerUnit : Unit
{
	public bool impassible = false;
	Animator anim;
	SpriteRenderer rend;
	GameObject gameMan;

	void Start(){
		rend = GetComponentInChildren<SpriteRenderer> ();
		anim = GetComponentInChildren<Animator> ();
		gameMan = GameObject.Find("GameManager");
	}

	// Update is called once per frame
	public override void Update ()
	{
		if (rend) {
			if (completeTurn) {
				rend.material.color = Color.grey;
			} else {
				rend.material.color = Color.white;
			}
		}
		if (anim && attackAnim) {
			anim.SetTrigger ("attack");
			attackAnim = false;
		}

		base.Update ();
	}

	public override void TurnUpdate ()
	{
		if (positionQueue.Count > 0) {
			transform.position += (positionQueue [0] - transform.position).normalized * moveSpeed * Time.deltaTime;

			if (Vector3.Distance (positionQueue [0], transform.position) <= 0.1f) {
				transform.position = positionQueue [0];
				positionQueue.RemoveAt (0);
			}
			if (anim) {
				anim.SetBool ("moving", true);
			} 
			if (positionQueue.Count == 0) {
				gameMan.GetComponent<AudioSource> ().Stop ();
			}

		} else if (anim) {
			anim.SetBool ("moving", false);
		}
		

		base.TurnUpdate ();
	}

	public override void TurnOnGUI ()
	{
		base.TurnOnGUI ();
	}

	void OnMouseDown ()
	{
		GameManager gm = GameManager.instance;

		if (gm.currentPlayerIndex >= 0 && gm.currentPlayerIndex < gm.map.units.Count) {
			if (gm.map.units [gm.currentPlayerIndex].attacking) {
				gm.AttackUnitWithCurrentPlayer (this);
			}
		}
		if (!completeTurn) {
			if (team == gm.currentPlayerTurn) {
				gm.clearSelection ();
				gm.removeTileHighlights ();
				selected = true;
			}
		}
	}
}
