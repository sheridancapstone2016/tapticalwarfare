﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameMap : MonoBehaviour{

	public string mapName;

	public int width = 6;
	public int height = 6;

	public List<List<Tile>> tiles = new List<List<Tile>>();
	public List<Unit> units = new List<Unit>();
	public List<Building> buildings = new List<Building>();

	static int lastId = 0;

	public int NextId{
		get{
			if (lastId == int.MaxValue) {
				lastId = 0;
			}
			lastId++;
			foreach (Building b in buildings) {
				if (b.id == lastId) {
					return NextId;
				}
			}
			foreach (Unit u in units) {
				if (u.id == lastId) {
					return NextId;
				}
			}
			return lastId;
		}
	}

	public int getUnitX(int index){
		return (int)units [index].gridCoordinates.x;
	}

	public int getUnitY(int index){
		return (int)units [index].gridCoordinates.y;
	}

	public Tile getTileByUnit(int unitIndex){
		return tiles[getUnitX (unitIndex)][getUnitY(unitIndex)];
	}

	public Tile getTileByPos(Vector2 pos){
		return tiles [(int)pos.x] [(int)pos.y];
	}

	public Vector2[] getUnitPositionMatrixForMove(int currentPlayerIndex, Tile destTile){
		Vector2[] positions = units.Where(
				x => x.gridCoordinates != destTile.gridCoordinates && x.gridCoordinates != units[currentPlayerIndex].gridCoordinates).Select(
					x => x.gridCoordinates
			).ToArray();
		return positions;
	}

	void centerCamera(){
		float centerY = height % 2 == 1 ? 0f : 0.5f;
		Camera cam = Camera.main;
		float xOffset = (width + 1) * 0.5f;

		if (height > width) {
			float size = (height + 1) * 0.5f;
			cam.orthographicSize = size;
		} else {
			cam.orthographicSize = xOffset;
		}

		xOffset -= xOffset * 0.25f;
		xOffset -= width % 2 == 1 ? 0f : 0.5f;

		cam.transform.localPosition = new Vector3 (xOffset, 20, centerY);
	}

	public void loadFromState(MapState state){
		mapName = state.mapName;
		height = state.height;
		width = state.width;

		centerCamera ();

		tiles.Clear ();
		buildings.Clear ();
		units.Clear ();

		#if !UNITY_EDITOR
		foreach (Transform child in transform) {
			if (!Application.isEditor) {
				Destroy (child.gameObject);
			}
		}
		#endif

		generateTiles ();
		generateUnitsAndBuildings(state);
	}

	void generateTiles(){
		tiles = new List<List<Tile>>();
		for (int i = 0; i < width; i++)
		{
			List<Tile> row = new List<Tile>();
			for (int j = 0; j < height; j++)
			{
				GameObject prefabObj = null;
				prefabObj = PrefabHolder.instance.TILE_PREFAB;
				Tile tile = ((GameObject)Instantiate(prefabObj, new Vector3(i - Mathf.Floor(width / 2), 0, -j + Mathf.Floor(height / 2)), Quaternion.Euler(new Vector3()))).GetComponent<Tile>();
				tile.transform.parent = transform;
				tile.gridCoordinates = new Vector2(i, j);
				row.Add(tile);
			}
			tiles.Add(row);
		}
	}

	void generateUnitsAndBuildings(MapState state)
	{
		foreach (MapState.MapPlayer.MapPlayerProperty p in state.player1.properties) {
			GameObject prefab = PrefabHolder.instance.getByName (p.type, 0);
			if (prefab) {
				createNewUnitOrBuilding (p.id, p.hp, p.x, p.y, 0, prefab);
			}
		}

		foreach (MapState.MapPlayer.MapPlayerProperty p in state.player2.properties) {
			GameObject prefab = PrefabHolder.instance.getByName (p.type, 1);
			if (prefab) {
				createNewUnitOrBuilding (p.id, p.hp, p.x, p.y, 1, prefab);
			}
		}
	}

	void createNewUnitOrBuilding(int x, int y, int team, GameObject prefab){
		createNewUnitOrBuilding (0, 0, x, y, team, prefab);
	}

	void createNewUnitOrBuilding(int id, int hp, int x, int y, int team, GameObject prefab){
		Vector3 pos = new Vector3();
		pos.x = x - Mathf.Floor(width / 2);
		pos.y = 1.5f;
		pos.z = -y + Mathf.Floor(height / 2);
		GameObject obj = ((GameObject)Instantiate(prefab, pos, transform.rotation, transform));

		PlayerBuilding pb = obj.GetComponent<PlayerBuilding>();
		PlayerUnit pu = obj.GetComponent<PlayerUnit>();

		if (id == 0) {
			id = NextId;
		} else if (id > lastId) {
			lastId = id;
		}

		if (pb) {
			if (hp != 0) {
				pb.HP = hp;
			}
			pb.id = id;
			pb.gridCoordinates = new Vector2 (x, y);
			pb.team = team;
			buildings.Add (pb);
		}
		if (pu) {
			if (hp != 0) {
				pu.HP = hp;
			}
			pu.id = id;
			pu.gridCoordinates = new Vector2 (x, y);
			pu.team = team;
			units.Add (pu);
		}
	}

	public void clearSelection(){
		for (int i = 0; i < units.Count(); i++)
		{
			units[i].selected = false;
			units[i].moving = false;
			units[i].attacking = false;
			units[i].building = false;
			units[i].back = false;
			units[i].back2 = false;
		}
		for (int i = 0; i < buildings.Count(); i++)
		{
			buildings[i].selected = false;
			buildings[i].attacking = false;
			buildings[i].building = false;
			buildings[i].back = false;
			buildings[i].back2 = false;
		}
	}

	public int getCurrentPlayerIndex(){
		for (int i = 0; i < units.Count(); i++)
		{
			if (units[i].selected == true)
			{
				return units.IndexOf(units[i]);
			}
		}
		for (int i = 0; i < buildings.Count(); i++)
		{
			if (buildings[i].selected == true)
			{
				return buildings.IndexOf(buildings[i]);
			}
		}
		return -1;
	}

	public MapState getMapState(){
		MapState ms = new MapState (mapName, height, width);

		List<MapState.MapPlayer.MapPlayerProperty> p1Props = new List<MapState.MapPlayer.MapPlayerProperty> ();
		List<MapState.MapPlayer.MapPlayerProperty> p2Props = new List<MapState.MapPlayer.MapPlayerProperty> ();

		foreach (Unit u in units) {
			MapState.MapPlayer.MapPlayerProperty prop = new MapState.MapPlayer.MapPlayerProperty ();
			prop.hp = u.HP;
			prop.id = u.id;
			prop.type = u.unitType.ToString ();
			prop.x = (int)u.gridCoordinates.x;
			prop.y = (int)u.gridCoordinates.y;
			if (u.team == 0) {
				p1Props.Add (prop);
			} else {
				p2Props.Add (prop);
			}
		}

		foreach (Building b in buildings) {
			MapState.MapPlayer.MapPlayerProperty prop = new MapState.MapPlayer.MapPlayerProperty ();
			prop.hp = b.HP;
			prop.id = b.id;
			prop.type = b.unitType.ToString ();
			prop.x = (int)b.gridCoordinates.x;
			prop.y = (int)b.gridCoordinates.y;
			if (b.team == 0) {
				p1Props.Add (prop);
			} else {
				p2Props.Add (prop);
			}
		}

		ms.player1.properties = p1Props.ToArray ();
		ms.player2.properties = p2Props.ToArray ();

		return ms;
	}
}
