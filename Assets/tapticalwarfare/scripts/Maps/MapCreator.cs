﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

public class MapCreator : MonoBehaviour {

	public MapState mapstate;
	GameMap map;

	public GameMap Map{
		get{
			if (!map) {
				map = GetComponentInChildren<GameMap> ();
			}
			return map;
		}
	}

	// Use this for initialization
	void Awake () {
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#endif
	}

	void OnApplicationQuit(){
		Debug.Log ("Map creator scene should not use playmode. Use the Build button in the Map Creator editor to view map.");
	}

	public void GenerateMap(){
		map.loadFromState (mapstate);
	}

	public string GenerateJSON(){
		if (mapstate != null) {
			mapstate.player1Resources = -1;
			mapstate.player2Resources = -1;
			return MapState.ToJSON (mapstate, true);
		}
		return null;
	}
}
