﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioClip construct; //For building a unit or building
    public AudioClip vehicleMove; //For builders, anti-airs, tanks
    public AudioClip mechMove; //For mechs
    public AudioClip aircraftMove; //For bombers and jets
    public AudioClip helicopterMove; //For helicopters
    public AudioClip gunShot; //For builders, mechs, helicopters
    public AudioClip missileShot; //For anti-airs, jets
    public AudioClip tankShot;//For tanks
    public AudioClip bomb; //For bombers
    public AudioClip insufficientFunds; //For when you try to build without enough funds

    public void PlayConstructSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = construct;
            asource.Play();
        }
    }
    
    public void PlayVehicleMoveSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = vehicleMove;
            asource.Play();
        }
    }

    public void PlayMechMoveSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = mechMove;
            asource.Play();
        }
    }
    
    public void PlayAircraftMoveSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = aircraftMove;
            asource.Play();
        }
    }
    
    public void PlayHelicopterMoveSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = helicopterMove;
            asource.Play();
        }
    }
    
    public void PlayGunShotSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = gunShot;
            asource.Play();
        }
    }

    public void PlayMissileShotSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = missileShot;
            asource.Play();
        }
    }
    
    public void PlayTankShotSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = tankShot;
            asource.Play();
        }
    }
    
    public void PlayBombSound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = bomb;
            asource.Play();
        }
    }
    
    public void PlayInsuffientFundsSound(AudioSource asource)
    {
        if (asource != null)
        {
			Debug.Log ("need more minerals");
            asource.clip = insufficientFunds;
            asource.Play();
        }
    }

    //Template
    /*
    public void PlaySound(AudioSource asource)
    {
        if (asource != null)
        {
            asource.clip = ;
            asource.Play();
        }
    }
    */

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
