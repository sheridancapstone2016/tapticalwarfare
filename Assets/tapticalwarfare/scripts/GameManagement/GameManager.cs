using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

//There are issues with deleting untis from the list outright. Every once and a while, take the units in the list, find out which ones
//are still alive using a boolean and put it in a new list. Possibly every time a turn ends.
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager instance = null;
	public static MapState mapToLoad = null;
	public static int gameWinner = -1;

	ResourceManager rM;
    SoundManager SM;
	AudioSource audi;
	public GameMap map;

    public int currentPlayerIndex = 0;
    public int unitOrBuilding = 0;
    public int currentPlayerTurn = 0;

    public Text health;
    public Text attack;
    public Text attackRange;
    public Text move;
    public Text victory;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
	{
		map = GetComponentInChildren<GameMap> ();
		audi = GetComponent<AudioSource> ();
		SM = GetComponent<SoundManager> ();

		if (mapToLoad == null) {
			Debug.Log ("mapToLoad is not set, loading default map");
			MapState state = MapState.FromFile ("Default.json");
			if (state != null) {
				map.loadFromState ((MapState)state);
			}
		} else {
			map.loadFromState (mapToLoad);
		}

		rM = this.GetComponent<ResourceManager>();
		if (rM) {
			rM.showMeTheMoney (currentPlayerTurn);
		}
        SM = this.GetComponent<SoundManager>();

		int startingPlayer = 0;
		if (OnlineContext.IsConnected && OnlineContext.currentGame != null) {
			startingPlayer = OnlineContext.currentGame.currentTurn;
		}
		if (mapToLoad.player1Resources >= 0) {
			rM.p0Resources = mapToLoad.player1Resources;
			rM.p1Resources = mapToLoad.player2Resources;
			rM.showMeTheMoney (startingPlayer);
		}

		mapToLoad = null;
        startComplete(startingPlayer);
    }
		
    // Update is called once per frame
    void Update()
    {
		if (currentPlayerIndex >= 0) {
			if (map.units.Count () > currentPlayerIndex) {
				if (map.units [currentPlayerIndex].HP > 0)
					map.units [currentPlayerIndex].TurnUpdate ();
			}
			if (map.buildings.Count () > currentPlayerIndex) {
				if (map.buildings [currentPlayerIndex].HP > 0)
					map.buildings [currentPlayerIndex].TurnUpdate ();
			}
		}
		//BRIAN ADD
        currentIndexUpdate();
        unitStatDisplay();
		if (isEndTurn()) {
			endTurn ();
		}
        rM.showMeTheMoney(currentPlayerTurn);
    }

    void OnGUI()
    {
		if (currentPlayerIndex > 0) {
			if (map.units.Count () > currentPlayerIndex) {
				if (map.units [currentPlayerIndex].HP > 0)
					map.units [currentPlayerIndex].TurnOnGUI ();
			}
			if (map.buildings.Count () > currentPlayerIndex) {
				if (map.buildings [currentPlayerIndex].HP > 0)
					map.buildings [currentPlayerIndex].TurnOnGUI ();
			}
		}

		Rect endTurnButtonRect = new Rect ();
		endTurnButtonRect.width = Screen.width * 0.25f;
		endTurnButtonRect.height = Screen.height * 0.15f;
		endTurnButtonRect.x = Screen.width - endTurnButtonRect.width - 50;
		endTurnButtonRect.y = Screen.height - endTurnButtonRect.height - 50;

		GUIStyle endTurnButtonStyle = new GUIStyle ("button");
		endTurnButtonStyle.fontSize = (int)(64f * (Screen.height / 700f));

		if (GUI.Button(endTurnButtonRect, "End Turn", endTurnButtonStyle))
        {
			endTurn ();
        }
    }

	bool isEndTurn(){
		foreach (Unit u in map.units) {
			if (u.dead) {
				continue;
			}
			if (u.team == currentPlayerTurn) {
				if (u.completeTurn == false)
					return false;
			}
		}
		foreach (Building b in map.buildings) {
			if (b.dead) {
				continue;
			}
			if (b.team == currentPlayerTurn) {
				if (b.unitType == UnitType.RE || b.unitType == UnitType.WA)
					continue;
				if (b.completeTurn == false) {
					return false;
				}
			}
		}
		return true;
	}

	public void endTurn(){
		GameManager.instance.removeTileHighlights();
		nextTurn();
        health.text = "";
        attack.text = "";
        attackRange.text = "";
        move.text = "";
		if (currentPlayerTurn == 0){
			//Player 1 units complete turn
			for (int i = 0; i < map.units.Count(); i++)
			{
				if (map.units[i].team == 0)
				{
					map.units[i].hasMoved = false;
					map.units[i].completeTurn = true;
				}
			}
			for (int i = 0; i < map.buildings.Count(); i++)
			{
				if (map.buildings[i].team == 0)
					map.buildings[i].completeTurn = true;
			}
			//Player 2 units can move again
			for (int i = 0; i < map.units.Count(); i++)
			{
				if (map.units[i].team == 1)
				{
					map.units[i].hasMoved = false;
					map.units[i].completeTurn = false;
				}
			}
			for (int i = 0; i < map.buildings.Count(); i++)
			{
				if (map.buildings[i].team == 1)
					map.buildings[i].completeTurn = false;
			}
			currentPlayerTurn = 1;
		}
		else
		{
			//Player 2 units complete turn
			for (int i = 0; i < map.units.Count(); i++)
			{
				if (map.units[i].team == 1)
				{
					map.units[i].hasMoved = false;
					map.units[i].completeTurn = true;
				}
			}
			for (int i = 0; i < map.buildings.Count(); i++)
			{
				if (map.buildings[i].team == 1)
					map.buildings[i].completeTurn = true;
			}
			//Player 1 units can move again
			for (int i = 0; i < map.units.Count(); i++)
			{
				if (map.units[i].team == 0)
				{
					map.units[i].hasMoved = false;
					map.units[i].completeTurn = false;
				}
			}
			for (int i = 0; i < map.buildings.Count(); i++)
			{
				if (map.buildings[i].team == 0)
					map.buildings[i].completeTurn = false;
			}
			currentPlayerTurn = 0;
		}

		if (OnlineContext.IsConnected) {
			OnlineContext.currentGame.currentTurn = currentPlayerTurn;
			MapState ms = map.getMapState ();
			ms.player1Resources = rM.p0Resources;
			ms.player2Resources = rM.p1Resources;
			OnlineContext.currentGame.gameState = ms.ToJSON ();
			SceneManager.LoadScene ("menuScene");
		}

		if (currentPlayerTurn == 0) {
			Camera.main.backgroundColor = new Color (0, 0, 0.25f);;
		} else {
			Camera.main.backgroundColor = new Color (0.25f, 0, 0);;
		}
		rM.nextTurn(currentPlayerTurn);
	}

    void startComplete(int startingPlayer)
    {
        //Player 2 units complete turn
		for (int i = 0; i < map.units.Count(); i++)
        {
			if (map.units[i].team != startingPlayer)
				map.units[i].completeTurn = true;
        }
        for (int i = 0; i < map.buildings.Count(); i++)
        {
			if (map.buildings[i].team != startingPlayer)
				map.buildings[i].completeTurn = true;
        }
        //Player 1 units can move again
		for (int i = 0; i < map.units.Count(); i++)
        {
			if (map.units[i].team == startingPlayer)
				map.units[i].completeTurn = false;
        }
		for (int i = 0; i < map.buildings.Count(); i++)
        {
			if (map.buildings[i].team == startingPlayer)
				map.buildings[i].completeTurn = false;
        }
        currentPlayerTurn = startingPlayer;

		rM.nextTurn (currentPlayerTurn);

		if (currentPlayerTurn == 0) {
			Camera.main.backgroundColor = new Color (0, 0, 0.25f);;
		} else {
			Camera.main.backgroundColor = new Color (0.25f, 0, 0);;
		}
    }

    public void nextTurn()
    {
		for (int i = 0; i < map.units.Count(); i++)
        {
			map.units[i].DoneMove = false;
			map.units[i].moving = false;
			map.units[i].attacking = false;
			map.units[i].building = false;
			map.units[i].back = false;
			map.units[i].back2 = false;
			map.units[i].selected = false;
        }
		for (int i = 0; i < map.buildings.Count(); i++)
        {
			map.buildings[i].DoneMove = false;
			map.buildings[i].attacking = false;
			map.buildings[i].building = false;
			map.buildings[i].back = false;
			map.buildings[i].back2 = false;
            map.buildings[i].back3 = false;
			map.buildings[i].selected = false;
        }
        currentPlayerIndex = 0;
    }
		
    public void highlightTilesAt(Vector2 originLocation, Color highlightColor, int distance)
    {
        highlightTilesAt(originLocation, highlightColor, distance, true);
    }

    public void highlightTilesAt(Vector2 originLocation, Color highlightColor, int distance, bool ignorePlayers)
    {

        List<Tile> highlightedTiles = new List<Tile>();

        if (ignorePlayers) highlightedTiles = TileHighlight.FindHighlight(map.tiles[(int)originLocation.x][(int)originLocation.y], distance, highlightColor == Color.red);
        else highlightedTiles = TileHighlight.FindHighlight(map.tiles[(int)originLocation.x][(int)originLocation.y], distance, map.units.Where(x => x.gridCoordinates != originLocation).Select(x => x.gridCoordinates).ToArray(), highlightColor == Color.red);

        foreach (Tile t in highlightedTiles)
        {
            t.transform.GetComponent<Renderer>().materials[0].color = highlightColor;
        }
    }

    public void removeTileHighlights()
    {
		for (int i = 0; i < map.width; i++)
        {
			for (int j = 0; j < map.height; j++)
            {
                map.tiles[i][j].transform.GetComponent<Renderer>().materials[0].color = Color.white;
            }
        }
    }


    public void moveCurrentPlayer(Tile destTile)
    {
		if (destTile.transform.GetComponent<Renderer>().materials[0].color != Color.white && !destTile.impassible && map.units[currentPlayerIndex].positionQueue.Count == 0)
        {
            removeTileHighlights();
			map.units[currentPlayerIndex].moving = false;
			map.units[currentPlayerIndex].back = false;

			//DETERMINE WHAT UNIT IS MOVING AND PLAYS THE CORRECT SOUND
			if (map.units [currentPlayerIndex].unitType == UnitType.JE || map.units [currentPlayerIndex].unitType == UnitType.AI || map.units [currentPlayerIndex].unitType == UnitType.BO) {
				SM.PlayAircraftMoveSound (audi);
			} else if (map.units [currentPlayerIndex].unitType == UnitType.AA || map.units [currentPlayerIndex].unitType == UnitType.TA || map.units [currentPlayerIndex].unitType == UnitType.BU) {
				SM.PlayVehicleMoveSound (audi);
			} else if (map.units [currentPlayerIndex].unitType == UnitType.HE) {
				SM.PlayHelicopterMoveSound (audi);
			} else if (map.units [currentPlayerIndex].unitType == UnitType.ME) {
				SM.PlayMechMoveSound (audi);
			}


			foreach (Tile t in TilePathFinder.FindPath(map.getTileByUnit(currentPlayerIndex), destTile, map.getUnitPositionMatrixForMove(currentPlayerIndex, destTile))){

				//TODO : IS IT REALLY NECESSARY TO GET TILE BY TILE'S X AND Y?
				map.units[currentPlayerIndex].positionQueue.Add(map.tiles[(int)t.gridCoordinates.x][(int)t.gridCoordinates.y].transform.position + 1.5f * Vector3.up);
            }
			map.units[currentPlayerIndex].gridCoordinates = destTile.gridCoordinates;
			map.units[currentPlayerIndex].DoneMove = true;
			map.units[currentPlayerIndex].hasMoved = true;
        }
        else
        {
            Debug.Log("destination invalid [Moving]");
        }
    }
	/*
    public void attackWithCurrentPlayer(Tile destTile)
    {
        if (destTile.transform.GetComponent<Renderer>().materials[0].color != Color.white && !destTile.impassible)
        {

            Unit target = null;
            foreach (Unit p in units)
            {
                if (p.gridCoordinates == destTile.gridCoordinates)
                {
                    target = p;
                }
            }
            Building target1 = null;
            foreach (Building q in buildings)
            {
                if (q.gridCoordinates == destTile.gridCoordinates)
                {
                    target1 = q;
                }
            }

            if (target != null)
            {
                //damage logic
                if (unitOrBuilding == 0)
                {
                    units[currentPlayerIndex].attacking = false;
                    units[currentPlayerIndex].back = false;
                    removeTileHighlights();

                    int amountOfDamage = (int)Mathf.Floor(units[currentPlayerIndex].attackDamage);
                    target.HP -= amountOfDamage;
                    if (target.HP <= 0)
                    {
                        target.gridCoordinates = new Vector2(-1, -1);
                        //destTile.impassible = true;
                        //Debug.Log(destTile.impassible);
                    }
                    units[currentPlayerIndex].attackAnim = true;
                    units[currentPlayerIndex].completeTurn = true;
                    units[currentPlayerIndex].finishTurn();
                }
                else
                {
                    buildings[currentPlayerIndex].attacking = false;
                    buildings[currentPlayerIndex].back = false;
                    removeTileHighlights();

                    int amountOfDamage = (int)Mathf.Floor(units[currentPlayerIndex].attackDamage);
                    target.HP -= amountOfDamage;
                    buildings[currentPlayerIndex].completeTurn = true;
                    buildings[currentPlayerIndex].completeTurn = true;
                    buildings[currentPlayerIndex].finishTurn();
                }
            }
        }
        else
        {
            Debug.Log("destination invalid (Attack)");
        }
    }
*/
	public void AttackUnitWithCurrentPlayer(Unit u)
	{
		Unit target = null;
        if (map.units[currentPlayerIndex].attacking)
        {
			if (map.getTileByPos(u.gridCoordinates).transform.GetComponent<Renderer>().materials[0].color != Color.white)
            {
                target = u;
            }
        }
		if (target != null && (map.units[currentPlayerIndex].team != target.team))
		{
			//damage logic
			map.units[currentPlayerIndex].attacking = false;
			map.units[currentPlayerIndex].back = false;
			removeTileHighlights();

			int amountOfDamage = (int)Mathf.Floor(map.units[currentPlayerIndex].attackDamage);
            //If the unit is a jet, it can only attack and do damage to other air units
            if (map.units[currentPlayerIndex].unitType == UnitType.JE)
            {
                if (target.groundOrAir)
                {
                    target.HP -= amountOfDamage;
                    if (target.HP <= 0)
                    {
                        target.gridCoordinates = new Vector2(-1, -1);
                    }
                    map.units[currentPlayerIndex].attackAnim = true;
					map.units [currentPlayerIndex].GetComponent<AudioSource> ().Play ();
                    map.units[currentPlayerIndex].completeTurn = true;
                    map.units[currentPlayerIndex].finishTurn();
                }
            }
            else if (map.units[currentPlayerIndex].unitType == UnitType.BO)
            {
                if (!target.groundOrAir)
                {
                    target.HP -= amountOfDamage;
                    if (target.HP <= 0)
                    {
                        target.gridCoordinates = new Vector2(-1, -1);
                    }
					map.units [currentPlayerIndex].GetComponent<AudioSource> ().Play ();
                    map.units[currentPlayerIndex].attackAnim = true;
                    map.units[currentPlayerIndex].completeTurn = true;
                    map.units[currentPlayerIndex].finishTurn();
                }
            }
            //If the unit is an anti-air unit, it will do triple damage to air units
            else if (map.units[currentPlayerIndex].unitType == UnitType.AA)
            {
                target.HP -= 3 * amountOfDamage;
                if (target.HP <= 0)
                {
                    target.gridCoordinates = new Vector2(-1, -1);
                }
				map.units [currentPlayerIndex].GetComponent<AudioSource> ().Play ();
                map.units[currentPlayerIndex].attackAnim = true;
                map.units[currentPlayerIndex].completeTurn = true;
                map.units[currentPlayerIndex].finishTurn();
            }
            else
            {
                target.HP -= amountOfDamage;
                if (target.HP <= 0)
                {
                    target.gridCoordinates = new Vector2(-1, -1);
                }
				map.units [currentPlayerIndex].GetComponent<AudioSource> ().Play ();
                map.units[currentPlayerIndex].attackAnim = true;
                map.units[currentPlayerIndex].completeTurn = true;
                map.units[currentPlayerIndex].finishTurn();
            }
		}
	}

	public void AttackBuildingWithCurrentPlayer(Building b)
	{
		Building target = null;
		if (map.units[currentPlayerIndex].attacking)
        {
			if (map.getTileByPos(b.gridCoordinates).transform.GetComponent<Renderer>().materials[0].color != Color.white)
            {
                target = b;
            }
        }
		if (target != null && (map.units[currentPlayerIndex].team != target.team))
		{
			target.attacking = false;
			target.back = false;
			removeTileHighlights();
			map.units [currentPlayerIndex].GetComponent<AudioSource> ().Play ();

			int amountOfDamage = (int)Mathf.Floor(map.units[currentPlayerIndex].attackDamage);
            //If a bomber attacks a building, it does twice the damage
            if (map.units[currentPlayerIndex].unitType == UnitType.BO)
            {
                target.HP -= 2 * amountOfDamage;
            }
            else
            {
                target.HP -= amountOfDamage;
            }
			
			//AttackMode = false;
            map.units[currentPlayerIndex].completeTurn = true;
            if (target.HP <= 0)
            {
                //When the HQ is destroyed (Unit type 0), end the game.
				if (target.unitType == UnitType.HQ)
                {
					if (target.team == 0)
                    {
                        victory.text = "Player 2 Wins!";
                        victory.color = Color.red;
						gameWinner = 1;
						SceneManager.LoadScene ("menuScene");
                    }
                    else
                    {
                        victory.text = "Player 1 Wins!";
                        victory.color = Color.blue;
						gameWinner = 0;
						SceneManager.LoadScene ("menuScene");
                    }
                }
                target.dead = true;
                target.gameObject.transform.position = new Vector3(-20, 20, -20);
            }
			map.units[currentPlayerIndex].attackAnim = true;
			target.finishTurn();
		}
	}

    public void buildWithCurrentPlayer(Tile destTile)
    {
        if (destTile.transform.GetComponent<Renderer>().materials[0].color != Color.white && !destTile.impassible)
        {
            if (unitOrBuilding == 0)
            {
                PlayerBuilding bld;

				UnitType type = map.units [currentPlayerIndex].construct;
				GameObject prefab = PrefabHolder.instance.getByName (type.ToString(), currentPlayerTurn);

				int price = PriceHolder.instance.getByPrefabType(type);

				if (rM.purchasable (price, currentPlayerTurn)) {
					bld = ((GameObject)Instantiate (prefab, new Vector3 (destTile.gridCoordinates.x - Mathf.Floor (map.width / 2), 1.5f, -destTile.gridCoordinates.y + Mathf.Floor (map.height / 2)), Quaternion.Euler (new Vector3 ()), map.transform)).GetComponent<PlayerBuilding> ();
					bld.id = map.NextId;
					bld.gridCoordinates = new Vector2 (destTile.gridCoordinates.x, destTile.gridCoordinates.y);
					bld.team = map.units [currentPlayerIndex].team;
					bld.completeTurn = true;
					map.buildings.Add (bld);
					rM.spend(price, currentPlayerTurn);
					if (bld.unitType == UnitType.RE) {
						rM.showMeTheMoney (currentPlayerTurn);
					}
					map.units[currentPlayerIndex].completeTurn = true;
					SM.PlayConstructSound (audi);
				}

				map.units[currentPlayerIndex].building = false;
				map.units[currentPlayerIndex].back = false;
				map.units[currentPlayerIndex].back2 = false;
				removeTileHighlights();
				map.units[currentPlayerIndex].finishTurn();

			}
			else
			{
				PlayerUnit player;

				UnitType type = map.buildings[currentPlayerIndex].construct;
				GameObject prefab = PrefabHolder.instance.getByName (type.ToString(), currentPlayerTurn);

				int price = PriceHolder.instance.getByPrefabType(type);

				if (rM.purchasable (price, currentPlayerTurn)) {
					player = ((GameObject)Instantiate (prefab, new Vector3 (destTile.gridCoordinates.x - Mathf.Floor (map.width / 2), 1.5f, -destTile.gridCoordinates.y + Mathf.Floor (map.height / 2)), Quaternion.Euler (new Vector3 ()), map.transform)).GetComponent<PlayerUnit> ();
					player.id = map.NextId;
					player.gridCoordinates = new Vector2 (destTile.gridCoordinates.x, destTile.gridCoordinates.y);
					player.team = map.buildings [currentPlayerIndex].team;
					player.completeTurn = true;
                    //Increase unit stats if the building has been upgraded
                    if (map.buildings[currentPlayerIndex].upgradeHealth)
                    {
                        player.HP += 5;
                    }
                    if (map.buildings[currentPlayerIndex].upgradeAttack)
                    {
                        player.attackDamage += 5;
                    }
                    if (map.buildings[currentPlayerIndex].upgradeMove)
                    {
                        player.movementRange += 1;
                    }
					map.units.Add (player);
					rM.spend(price, currentPlayerTurn);
					map.buildings[currentPlayerIndex].completeTurn = true;
				}

				map.buildings[currentPlayerIndex].building = false;
				map.buildings[currentPlayerIndex].back = false;
				map.buildings[currentPlayerIndex].back2 = false;
                map.buildings[currentPlayerIndex].back3 = false;
                removeTileHighlights();
				map.buildings[currentPlayerIndex].finishTurn();
            }
        }
        else
        {
            Debug.Log("Destination Invalid (Building)");
        }
    }

    public void clearSelection()
    {
		map.clearSelection ();
    }

    public void currentIndexUpdate()
    {
       currentPlayerIndex = map.getCurrentPlayerIndex();
    }

    //This method will display a unit's stats if they are selected
    public void unitStatDisplay(){
        if (unitOrBuilding == 0)
        {
            if (currentPlayerIndex < map.units.Count() && currentPlayerIndex >= 0)
            {
                health.text = map.units[currentPlayerIndex].HP.ToString();
                attack.text = map.units[currentPlayerIndex].attackDamage.ToString();
                attackRange.text = map.units[currentPlayerIndex].attackRange.ToString();
                move.text = map.units[currentPlayerIndex].movementRange.ToString();
            }
        }
        else
        {
            health.text = "";
            attack.text = "";
            attackRange.text = "";
            move.text = "";
        }
    }
}
