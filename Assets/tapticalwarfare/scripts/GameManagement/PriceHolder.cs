﻿using UnityEngine;
using System.Collections;

public class PriceHolder : MonoBehaviour {
	public static PriceHolder instance;

	public int builderPrice = 500;
	public int antiairPrice = 1000;
	public int mechPrice = 1000;
	public int tankPrice = 1500;
	public int bomberPrice = 2000;
	public int helicopterPrice = 1500;
	public int jetPrice = 1750;

	public int headquartersPrice = 0;
	public int resourcePrice = 500;
	public int factoryPrice = 500;
	public int airportPrice = 750;
	public int wallPrice = 100; //Alternatively 0, because we're going to build a wall and HAVE MEXICO PAY FOR IT!

	void Awake(){
		instance = this;
	}

	public int getByPrefabType(UnitType type){
		int price = 0;
		switch (type) {
		case UnitType.AA:
			price = PriceHolder.instance.antiairPrice;
			break;
		case UnitType.AI:
			price = PriceHolder.instance.airportPrice;
			break;
		case UnitType.BO:
			price = PriceHolder.instance.bomberPrice;
			break;
		case UnitType.BU:
			price = PriceHolder.instance.builderPrice;
			break;
		case UnitType.FA:
			price = PriceHolder.instance.factoryPrice;
			break;
		case UnitType.HE:
			price = PriceHolder.instance.helicopterPrice;
			break;
		case UnitType.HQ:
			price = PriceHolder.instance.headquartersPrice;
			break;
		case UnitType.JE:
			price = PriceHolder.instance.jetPrice;
			break;
		case UnitType.ME:
			price = PriceHolder.instance.mechPrice;
			break;
		case UnitType.RE:
			price = PriceHolder.instance.resourcePrice;
			break;
		case UnitType.TA:
			price = PriceHolder.instance.tankPrice;
			break;
		case UnitType.WA:
			price = PriceHolder.instance.wallPrice;
			break;
		}
		return price;
	}
}
