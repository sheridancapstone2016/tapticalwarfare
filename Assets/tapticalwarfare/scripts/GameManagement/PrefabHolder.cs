﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PrefabHolder : MonoBehaviour {
	public static PrefabHolder instance;

	public GameObject TILE_PREFAB;

	public GameObject builder;
	public GameObject antiair;
	public GameObject mech;
	public GameObject tank;
	public GameObject bomber;
	public GameObject helicopter;
	public GameObject jet;

	public GameObject headquarters;
	public GameObject resource;
	public GameObject factory;
	public GameObject airport;
	public GameObject wall;

	public GameObject builder1;
	public GameObject antiair1;
	public GameObject mech1;
	public GameObject tank1;
	public GameObject bomber1;
	public GameObject helicopter1;
	public GameObject jet1;

	public GameObject headquarters1;
	public GameObject resource1;
	public GameObject factory1;
	public GameObject airport1;
	public GameObject wall1;



	void Awake() {
		instance = this;
	}

	void Update(){
		if (!instance) {
			Awake ();
		}
	}

	public GameObject getByName(string type, int team){
		type = type.ToUpper ();
		if (type == UnitType.BU.ToString()) {
			if (team == 0) {
				return builder;
			} else if (team == 1) {
				return builder1;
			}
		} else if (type == UnitType.AA.ToString()) {
			if (team == 0) {
				return antiair;
			} else if (team == 1) {
				return antiair1;
			}
		} else if (type == UnitType.ME.ToString()) {
			if (team == 0) {
				return mech;
			} else if (team == 1) {
				return mech1;
			}
		} else if (type == UnitType.TA.ToString()) {
			if (team == 0) {
				return tank;
			} else if (team == 1) {
				return tank1;
			}
		} else if (type == UnitType.BO.ToString()) {
			if (team == 0) {
				return bomber;
			} else if (team == 1) {
				return bomber1;
			}
		} else if (type == UnitType.HE.ToString()) {
			if (team == 0) {
				return helicopter;
			} else if (team == 1) {
				return helicopter1;
			}
		} else if (type == UnitType.JE.ToString()) {
			if (team == 0) {
				return jet;
			} else if (team == 1) {
				return jet1;
			}
		} else if (type == UnitType.HQ.ToString()) {
			if (team == 0) {
				return headquarters;
			} else if (team == 1) {
				return headquarters1;
			}
		} else if (type == UnitType.RE.ToString()) {
			if (team == 0) {
				return resource;
			} else if (team == 1) {
				return resource1;
			}
		} else if (type == UnitType.FA.ToString()) {
			if (team == 0) {
				return factory;
			} else if (team == 1) {
				return factory1;
			}
		} else if (type == UnitType.AI.ToString()) {
			if (team == 0) {
				return airport;
			} else if (team == 1) {
				return airport1;
			}
		} else if (type == UnitType.WA.ToString()) {
			if (team == 0) {
				return wall;
			} else if (team == 1) {
				return wall1;
			}
		} 
		Debug.Log ("No prefab for type: " + type.ToString());
		return null;
	}
}
