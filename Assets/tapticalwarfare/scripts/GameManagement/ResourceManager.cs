﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour {
	public GameManager gm;
	public SoundManager sm;
	public int p0Resources = 500;
	public int p1Resources = 500;
	AudioSource audi;
	public int refineryBonus = 250;

	public int turnBonus = 250;

	public int totalBonus;

	public Text resourceDisplay;
	public Text turnDisplay;
	public Text profitDisplay;

	public float timeLeft;

	public int getPayRate(int team){
		int payRate = turnBonus;
		foreach (Building b in gm.map.buildings) {
			if (b.team == team && b.unitType == UnitType.RE) {
                if (b.HP > 0)
                {
                    payRate += refineryBonus;
                }
			}
		}
		return payRate;
	}

	// Use this for initialization
	void Start () {
		gm = GameManager.instance;
		sm = GetComponent<SoundManager> ();
		audi = GetComponent<AudioSource> ();
	}

	//Turns money Text red when you don't have the money
	public void notEnoughMinerals()
	{
		timeLeft = 1.0f;		
		sm.PlayInsuffientFundsSound (audi);

		Debug.Log ("hi");
	}

	// Update is called once per frame
	void Update () {
		if (timeLeft > -5) {
			timeLeft -= Time.deltaTime;
		}
		if (timeLeft > 0) {
			resourceDisplay.color = Color.red;
		} else if (timeLeft <= 0) {
			resourceDisplay.color = Color.white;
		}
	}

	public void nextTurn(int team)
	{
		if (team == 0) {
			p0Resources += getPayRate(0);

		} else if (team == 1) {
			p1Resources += getPayRate(1);
		}
		showMeTheMoney (team);
	}

	public void spend(int unitCost, int team)
	{
		if (team == 0) {
			if (unitCost <= p0Resources) {
				p0Resources -= unitCost;
				showMeTheMoney (team);
			} 
		} else if (team == 1) {
			if (unitCost <= p1Resources) {
				p1Resources -= unitCost;
				showMeTheMoney (team);
			}
		}
	}

	public bool purchasable(int unitCost, int team)
	{
		if (team == 0) {
			if (unitCost <= p0Resources) {
				return true;
			} else {
				notEnoughMinerals ();
				return false;
			}
		} else if (team == 1) {
			if (unitCost <= p1Resources) {
				return true;
			} else {
				notEnoughMinerals ();
				return false;
			}
		}
		notEnoughMinerals ();
		return false;
	}

	public void showMeTheMoney(int team)
	{
		if (team == 0) {
			resourceDisplay.text = p0Resources.ToString();
		} else if (team == 1) {
			resourceDisplay.text = p1Resources.ToString();
		}
		profitDisplay.text = getPayRate (team).ToString();
		turnDisplay.text = "Player " + (team + 1);
	}
}
