using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TileHighlight {
	
	public TileHighlight () {
		
	}

	public static List<Tile> FindHighlight(Tile originTile, int movementPoints) {
		return FindHighlight(originTile, movementPoints, new Vector2[0], false);
	}
	public static List<Tile> FindHighlight(Tile originTile, int movementPoints, bool staticRange) {
		return FindHighlight(originTile, movementPoints, new Vector2[0], staticRange);
	}
	public static List<Tile> FindHighlight(Tile originTile, int movementPoints, Vector2[] occupied) {
		return FindHighlight(originTile, movementPoints, occupied, false);
	}

	public static List<Tile> FindHighlight(Tile originTile, int movementPoints, Vector2[] occupied, bool staticRange) {
		GameManager gm = GameManager.instance;

		List<Tile> closed = new List<Tile>();
		List<TilePath> open = new List<TilePath>();
		
		TilePath originPath = new TilePath();
		if (staticRange) originPath.addStaticTile(originTile);
		else originPath.addTile(originTile);
		
		open.Add(originPath);
		
		while (open.Count > 0) {
			TilePath current = open[0];
			open.Remove(open[0]);
			
			if (closed.Contains(current.lastTile)) {
				continue;
			} 
			if (current.costOfPath > movementPoints + 1) {
				continue;
			}

			closed.Add(current.lastTile);
            if (GameManager.instance.unitOrBuilding == 0)
            {
				if (gm.map.units[GameManager.instance.currentPlayerIndex].attacking)
                {
                    foreach (Tile t in current.lastTile.neighbors)
                    {
                        if (t.impassible || occupied.Contains(t.gridCoordinates)) continue;
                        TilePath newTilePath = new TilePath(current);
                        newTilePath.addTile(t);
                        open.Add(newTilePath);
                    }
                }
                else
                {
                    if (GameManager.instance.map.units[GameManager.instance.currentPlayerIndex].groundOrAir)
                    {
                        foreach (Tile t in current.lastTile.neighbors)
                        {
                            //if (occupied.Contains(t.gridCoordinates)) continue;
                            TilePath newTilePath = new TilePath(current);
                            newTilePath.addTile(t);
                            open.Add(newTilePath);
                        }
                    }
                    else
                    {
                        foreach (Tile t in current.lastTile.neighbors)
                        {
                            if (t.impassible || t.ShallNotPass || occupied.Contains(t.gridCoordinates)) continue;
                            TilePath newTilePath = new TilePath(current);
                            newTilePath.addTile(t);
                            open.Add(newTilePath);
                        }
                    }
                }
            }
            else
            {
                foreach (Tile t in current.lastTile.neighbors)
                {
                    if (t.impassible || t.ShallNotPass || occupied.Contains(t.gridCoordinates)) continue;
                    TilePath newTilePath = new TilePath(current);
                    newTilePath.addTile(t);
                    open.Add(newTilePath);
                }
            }
		}
		closed.Remove(originTile);
		closed.Distinct();
		return closed;
	}
}
