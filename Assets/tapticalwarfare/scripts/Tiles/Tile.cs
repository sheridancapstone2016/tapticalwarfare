using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Tile : MonoBehaviour
{
	GameObject PREFAB;

	public Vector2 gridCoordinates = Vector2.zero;
	
	public int movementCost = 1;
	public bool impassible = false;
	public bool ShallNotPass = false;

	public List<Tile> neighbors = new List<Tile> ();

	// Use this for initialization
	void Start ()
	{
		generateNeighbors ();
	}

	public void generateNeighbors ()
	{		
		GameManager gm = GameManager.instance;
		neighbors = new List<Tile> ();
		
		//up
		if (gridCoordinates.y > 0) {
			Vector2 n = new Vector2 (gridCoordinates.x, gridCoordinates.y - 1);
			neighbors.Add (gm.map.getTileByPos(n));
		}
		//down
		if (gridCoordinates.y < gm.map.height - 1) {
			Vector2 n = new Vector2 (gridCoordinates.x, gridCoordinates.y + 1);
			neighbors.Add (gm.map.getTileByPos(n));
		}		
		
		//left
		if (gridCoordinates.x > 0) {
			Vector2 n = new Vector2 (gridCoordinates.x - 1, gridCoordinates.y);
			neighbors.Add (gm.map.getTileByPos(n));
		}
		//right
		if (gridCoordinates.x < gm.map.width - 1) {
			Vector2 n = new Vector2 (gridCoordinates.x + 1, gridCoordinates.y);
			neighbors.Add (gm.map.getTileByPos(n));
		}
	}

	void OnMouseExit ()
	{

	}

	void OnMouseDown ()
	{ 
		GameManager gm = GameManager.instance;

		//Unit move/attack/building
		//This is to check which unit or building we need to use. If there are more units than buildings and we try to access a unit that has an index higher than
		//the total amount of buildings, we'll get an out of bounds index error.
		if (gm.unitOrBuilding == 0 && gm.currentPlayerIndex != -1) {
			if (gm.map.units [gm.currentPlayerIndex].moving) {
				gm.moveCurrentPlayer (this);
			} else if (gm.map.units [GameManager.instance.currentPlayerIndex].building) {
				gm.buildWithCurrentPlayer (this);
			}
		} else {
			if (gm.currentPlayerIndex != -1) {
				if (gm.map.buildings [gm.currentPlayerIndex].building) {
					gm.buildWithCurrentPlayer (this);
				}
			}
		}

	}
}
