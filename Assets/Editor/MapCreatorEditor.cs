﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

[CustomEditor (typeof(MapCreator))]
public class MapCreatorEditor : Editor
{
	const string FILE_PATH = "Assets/Resources/" + MapState.MAP_DIR + "[FILENAME].json";

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		MapCreator myTarget = (MapCreator)target;

		if(GUILayout.Button("Build")){
			List<GameObject> obj = new List<GameObject> ();
			foreach (Transform child in myTarget.Map.transform) {
				obj.Add (child.gameObject);
			}
			foreach (GameObject o in obj) {
				DestroyImmediate (o);
			}
			myTarget.GenerateMap ();
		}

		if (GUILayout.Button ("Save New")) {
			string filePath = getFilePath (myTarget.mapstate.mapName);
			if(filePath != null){
				if (!File.Exists(filePath)) {
					StreamWriter writer = new StreamWriter(filePath, false);
					writer.WriteLine(myTarget.GenerateJSON());
					writer.Close ();
				} else {
					Debug.LogError ("Failed to save file: File already exists! Use Update to overwrite file.");
				}
			} else {
				Debug.LogError ("Failed to save file: Map Name is empty!");
			}
		}

		if (GUILayout.Button ("Load")) {
			string filePath = getFilePath (myTarget.mapstate.mapName);
			if(filePath != null){
				if (File.Exists(filePath)) {
					StreamReader reader = new StreamReader (filePath);
					string fileContent = reader.ReadToEnd();
					reader.Close ();
					MapState ms = MapState.FromJSON (fileContent);
					if (ms != null) {
						myTarget.mapstate = ms;
						//rebuild
						List<GameObject> obj = new List<GameObject> ();
						foreach (Transform child in myTarget.Map.transform) {
							obj.Add (child.gameObject);
						}
						foreach (GameObject o in obj) {
							DestroyImmediate (o);
						}
						myTarget.GenerateMap ();
					} else {
						Debug.LogError ("Failed to load file: File content was invalid!");
					}
				} else {
					Debug.LogError ("Failed to load file: File does not exist!");
				}
			} else {
				Debug.LogError ("Failed to load file: Map Name is empty!");
			}
		}

		if (GUILayout.Button ("Save/Update")) {
			string filePath = getFilePath (myTarget.mapstate.mapName);
			if(filePath != null){
				StreamWriter writer = new StreamWriter(filePath, false);
				writer.WriteLine(myTarget.GenerateJSON());
				writer.Close ();
			} else {
				Debug.LogError ("Failed to save file: Map Name is empty!");
			}
		}
			
		EditorGUILayout.HelpBox("The Save New, Load, and Save/Update functions will not work if 'Map Name' is empty.", MessageType.Info);
	}

	string getFilePath(string name){
		string fileName = name.Trim().Replace(".json", "");
		if (fileName.Length > 0) {
			fileName = FILE_PATH.Replace ("[FILENAME]", fileName);
			return fileName;
		}
		return null;
	}
}
